# ENTREGA CONVOCATORIA JUNIO
# Entrega practica
## Datos
* Nombre:Ruben Benavente   
* Titulación: Tecnologías de la Telecomunicación
* Cuenta en laboratorios: rbenaven
* Cuenta URJC: r.benavente.2018@alumnos.urjc.es
* Video básico (url): https://youtu.be/YUPFk77zjyA
* Video parte opcional (url): https://youtu.be/3poNLng2jUo
* Despliegue (url): http://2796017eca85.sn.mynetname.net:8989/
* Contraseñas: 12345
* Cuenta Admin Site: admin/admin
## Resumen parte obligatoria
Para la parte obligatoria se cumplen todos los requisitos. Empezamos con un login que recibirá una única 
contraseña que de ser correcta generará una cookie de sesión que nos autorizará al resto de recursos, es decir antes del login cual página nos redirigirá al login.
Una vez dentro apareceremos en la página principal donde veremos las salas creadas y cuantos mensajes tiene cada una además de los mensajes desde la última vez que entramos,
también veremos el nombre del usuario actual y un formulario para crear páginas que en el caso de que ya existan no la creará de nuevo si no que nos llevará a la existente.
Dentro de cada sala podremos enviar mensajes especificando si son una imagen o no y ver el resto de mensajes que se han mandado con anterioridad,
al fondo tendremos 2 botones para ir a la sala en formato JSON y la sala dinámica que se actualizará cada 5 segundos. A las salas les podremos enviar mensajes a parte de por el 
formulario que hay en las salas también mediante un PUT del mensaje tanto si estamos logueados como si no siempre que en el segundo caso incluyamos la contraseña de la cabecera, siempre
con el formato especificado en el enunciado. Luego tenemos la página de configuración en la que podremos cambiar el estilo de fuente y el tamaño de toda la web asi como el nombre del usuario
que tambien actualizará su nombre en los mensajes que envio con anterioridad. Luego tenemos la página ayuda con información y por último el admin de django 
en el que podremos manejar las bases de datos de los modelos. También se incluye el pie de página con las salas y los mensajes tanto de texto como las imágenes y los accesos directos en la cabecera
tambien incluidos en el base.html. Se incluyen tests de todos los recursos. El despliegue se hará en una Raspberry Pi 3b.
## Lista partes opcionales
* Botón finalizar sesión: Se incluye un botón presente en todas las páginas que borra la cookie sesión obligando a iniciarla de nuevo 
* Favicon: Se incluye un favicon.
* Botón de Like: en la página principal se incluye un botón de like y aparece un registro de todos los likes. Solo es posible uno por sesion_key.
* Traducción: La página se traducirá dependiendo del idioma del navegador. Las traducciones están en dos archivos localizados en locale. **IMPORTANTE** Para este apartado necesité instalar gettext para crear los archivos de traducción (sudo apt-get install gettext) **NO** lo incluyo en requirements al no ser un módulo PYTHON pero me fue necesario para la creacón de estos archivos.
* Scrapping: Se realiza scrapping de la página web Marca.com para obtener todos los titulares guardarlos en formato XML incluyendo la cabecera de autenticación y después enviarlos mediante un PUT en una sala cada titular en un mensaje separado. Esto solo funciona con esta web.