"""PracticaFinal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.template.context_processors import static
# urls.py
from django.urls import path

from PracticaFinal import settings
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('config/', views.config, name='config'),
    path('help/', views.help_page, name='help'),
    path('login/', views.login, name='login'),
    path('scrapping/', views.scraping_view, name='scrapping'),
    path('scraping_process/', views.scraping_process_view, name='scraping_process'),
    path('sign_out/', views.sign_out, name='sign_out'),
    path('<str:sala_nombre>/', views.room, name='sala'),
    path('<str:sala_nombre>/like/', views.like_sala, name='like_sala'),
    path('crear_sala', views.crear_sala, name='crear_sala'),
    path('<str:sala_nombre>/din/', views.dynamic_room, name='dynamic_room'),
    path('<str:sala_nombre>/json/', views.sala_json, name='json_room'),

]
