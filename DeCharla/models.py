# Create your models here.
from django.utils import timezone

from django.db import models


class Sala(models.Model):
    nombre = models.CharField(max_length=100)
    autor = models.CharField(max_length=255)
    session_key = models.CharField(max_length=255)

    def __str__(self):
        return self.nombre


class VotoSala(models.Model):
    sala = models.ForeignKey(Sala, on_delete=models.CASCADE, related_name='votos')
    session_key = models.CharField(max_length=255)

    def __str__(self):
        return f"Voto de sesión {self.session_key}"

    class Meta:
        unique_together = ('sala', 'session_key')


Sala.votos = property(lambda self: VotoSala.objects.filter(sala=self))


class Mensaje(models.Model):
    sala = models.ForeignKey(Sala, related_name='mensajes', on_delete=models.CASCADE)
    texto = models.TextField()
    es_imagen = models.BooleanField(default=False, null=True)
    fecha = models.DateTimeField(default=timezone.now)
    autor = models.CharField(max_length=255)
    session_key = models.CharField(max_length=255)

    def __str__(self):
        return self.texto


class Passwords(models.Model):
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.password


class ValidSession(models.Model):
    usuario = models.CharField(max_length=255)
    session_key = models.CharField(max_length=255)
    font_size = models.CharField(max_length=10, default='medium')
    font_type = models.CharField(max_length=20, default='arial')

    def __str__(self):
        return self.usuario

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # Obtener mensajes y salas del usuario con el mismo session_key
        mensajes = Mensaje.objects.filter(session_key=self.session_key)
        salas = Sala.objects.filter(session_key=self.session_key)

        # Actualizar mensajes y salas con el nuevo nombre de usuario
        mensajes.update(autor=self.usuario)
        salas.update(autor=self.usuario)
