��    6      �  I   |      �     �     �     �     �     �     �     �     �     �               #  
   )     4     =  �  L  V   0  �   �     �  1   �     �     �     �     �     �     	     	     -	     D	     \	     e	     m	     �	     �	     �	     �	     �	  (   �	     
     0
  	   9
     C
     P
     i
     w
  $   �
  	   �
     �
     �
     �
     �
  	   �
  	     D       S     Y     _     e     l     u     {     �     �     �     �     �  
   �     �     �  �  �  V   �  �   9     9  1   H     z     �     �     �     �     �     �     �     �                    :     V     _     y     �  (   �     �     �  	   �     �               )  $   E  	   j     t     �     �     �  	   �  	   �            *   -      %   "               .      
               )   6   2                  3   $   &            	                                     (              ,              1      !       0             /             #      +   5          4      '           Admin Arial Autor Autor: Autoría Ayuda Ayuda - DeCharla Cargar página en formato JSON Cerrar sesión Configuración Courier New Crear Crear Sala DeCharla Documentación En esta práctica se desarrolló una aplicación de sala de chat dinámica utilizando Django. Los usuarios pueden crear salas, enviar mensajes y votar las salas existentes. Se implementó internacionalización para admitir la traducción del contenido al idioma preferido del usuario. Se utilizó CSS y Bootstrap para mejorar el diseño de la interfaz de usuario. En resumen, se construyó una plataforma interactiva de chat con funcionalidades básicas y personalización de idioma. En esta versión solo está disponible https://www.marca.com/. Disculpe las molestias. En este foro puedes iniciar sesión mediante una contraseña común a todos los usuarios, a raíz de esto podrás crear salas e interaccionar en las salas ya creadas con el usuario por defecto 'anonimo' o cambiarte el nombre para identificar tus mensajes. Enviar Mensaje Este foro ha sido realizado por Rubén Benavente. Fantasy Funcionamiento Grande Guardar Ingrese el mensaje Ingrese la URL de la imagen Iniciar Sesión Ir a la Sala Dinámica Marcar si es una imagen Me gusta Mediana Mensaje o URL de la imagen Mensaje o URL de la imagen: Mensajes No hay salas disponibles. Nombre de Charlador Nombre de la sala Número de mensajes desde última visita Número total de mensajes Pequeña Principal Sala de Chat Sala de Chat Dinámica - Salas activas Scrapear e Enviar a la Sala Scraping de Información (Marca.com) Scrapping Times New Roman URL para Scraping: Usuario actual Votos imágenes textuales Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Admin Arial Autor Autor: Autoría Ayuda Ayuda - DeCharla Cargar página en formato JSON Cerrar sesión Configuración Courier New Crear Crear Sala DeCharla Documentación En esta práctica se desarrolló una aplicación de sala de chat dinámica utilizando Django. Los usuarios pueden crear salas, enviar mensajes y votar las salas existentes. Se implementó internacionalización para admitir la traducción del contenido al idioma preferido del usuario. Se utilizó CSS y Bootstrap para mejorar el diseño de la interfaz de usuario. En resumen, se construyó una plataforma interactiva de chat con funcionalidades básicas y personalización de idioma. En esta versión solo está disponible https://www.marca.com/. Disculpe las molestias. En este foro puedes iniciar sesión mediante una contraseña común a todos los usuarios, a raíz de esto podrás crear salas e interaccionar en las salas ya creadas con el usuario por defecto 'anonimo' o cambiarte el nombre para identificar tus mensajes. Enviar Mensaje Este foro ha sido realizado por Rubén Benavente. Fantasy Funcionamiento Grande Guardar Ingrese el mensaje Ingrese la URL de la imagen Iniciar Sesión Ir a la Sala Dinámica Marcar si es una imagen Me gusta Mediana Mensaje o URL de la imagen Mensaje o URL de la imagen: Mensajes No hay salas disponibles. Nombre de Charlador Nombre de la sala Número de mensajes desde última visita Número total de mensajes Pequeña Principal Sala de Chat Sala de Chat Dinámica - Salas activas Scrapear e Enviar a la Sala Scraping de Información (Marca.com) Scrapping Times New Roman URL para Scraping: Usuario actual Votos imágenes textuales 