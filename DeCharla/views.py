# views.py
import os
from datetime import datetime
from django.http import JsonResponse
import requests
from django.http.response import HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from requests.exceptions import RequestException
from validators import url as validate_url
from django.shortcuts import render, redirect, get_object_or_404, reverse
from .forms import ConfigForm, PasswordForm
from .models import ValidSession, Sala, Mensaje, Passwords
import secrets
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
from django.utils.translation import gettext_lazy as _


def like_sala(request, sala_nombre):
    if request.method == 'POST':
        session_key = request.COOKIES.get('session_key')
        if session_key and not Sala.objects.filter(nombre=sala_nombre, votos__session_key=session_key).exists():
            sala = get_object_or_404(Sala, nombre=sala_nombre)
            sala.votos.create(session_key=session_key, sala=sala)
    return redirect('home')


def sign_out(request):
    if 'session_key' in request.COOKIES:
        response = redirect('login')
        response.delete_cookie('session_key')
        return response
    else:
        return redirect('login')


def get_metrics():
    total_mensajes_textuales = Mensaje.objects.filter(es_imagen=False).count()
    total_mensajes_imagenes = Mensaje.objects.filter(es_imagen=True).count()
    total_salas_activas = Sala.objects.filter(mensajes__isnull=False).distinct().count()

    return total_mensajes_textuales, total_mensajes_imagenes, total_salas_activas


def session_required(view_func):
    def _wrapped_view(request, *args, **kwargs):
        if 'session_key' in request.COOKIES:
            session_key = request.COOKIES['session_key']
            if ValidSession.objects.filter(session_key=session_key).exists():
                return view_func(request, *args, **kwargs)

        if request.method == 'PUT':
            authorization_header = request.headers.get('Authorization')
            if authorization_header:
                password = authorization_header.split(' ')[-1]
                valid_passwords = Passwords.objects.values_list('password', flat=True)
                if password in valid_passwords:
                    session_key = secrets.token_hex(16)
                    ValidSession.objects.create(usuario='anonimo', session_key=session_key)
                    return view_func(request, session_key=session_key, *args, **kwargs)
        return redirect('login')

    return _wrapped_view


@session_required
def scraping_view(request):
    usuario_actual = None
    session_key = request.COOKIES.get('session_key')
    if session_key is not None and ValidSession.objects.filter(session_key=session_key).exists():
        usuario_actual = ValidSession.objects.get(session_key=session_key)
    total_mensajes_textuales, total_mensajes_imagenes, total_salas_activas = get_metrics()
    context = {
        'usuario_actual': usuario_actual,
        'total_mensajes_textuales': total_mensajes_textuales,
        'total_mensajes_imagenes': total_mensajes_imagenes,
        'total_salas_activas': total_salas_activas,
    }
    return render(request, 'scrapping.html', context)


def scraping_process_view(request):
    if request.method == 'POST':
        url = request.POST.get('url')
        sala_nombre = request.POST.get('sala_nombre')

        if url != 'https://www.marca.com/':
            return render(request, 'scrapping.html', {
                'error': _('En esta versión solo está disponible https://www.marca.com/. Disculpe las molestias.')
            })

        try:
            sala = Sala.objects.get(nombre=sala_nombre)
        except Sala.DoesNotExist:
            sala = Sala.objects.create(nombre=sala_nombre)

        response = requests.get(url)

        soup = BeautifulSoup(response.content, "html.parser")

        headlines = soup.find_all("h2", class_="ue-c-cover-content__headline")

        sala_url = request.build_absolute_uri(reverse('sala', args=[sala.nombre]))

        root = ET.Element("messages")

        for headline in headlines:
            mensaje_texto = ET.SubElement(root, "message", isimg="false")
            texto_texto = ET.SubElement(mensaje_texto, "text")
            texto_texto.text = headline.text

        tree = ET.ElementTree(root)

        file_path = "static/mensajes.xml"
        tree.write(file_path, encoding="utf-8", xml_declaration=True)

        with open(file_path, "rb") as file:
            xml_content = file.read()

        headers = {"Authorization": "Basic 12345"}

        requests.put(sala_url, data=xml_content, headers=headers)

        os.remove(file_path)

        return redirect('sala', sala_nombre=sala.nombre)

    return render(request, 'scrapping.html', {'error': 'Error en la solicitud'})


@session_required
def home(request):
    salas = Sala.objects.all()
    ultima_visita = request.session.get('ultima_visita', datetime(1970, 1, 1))

    if 'ultima_visita' in request.session:
        ultima_visita = request.session['ultima_visita']

    request.session['ultima_visita'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    salas_con_mensajes = []
    for sala in salas:
        mensajes_desde_ultima_visita = sala.mensajes.filter(fecha__gt=ultima_visita)
        numero_mensajes = mensajes_desde_ultima_visita.count()
        salas_con_mensajes.append((sala, numero_mensajes))

    usuario_actual = None
    session_key = request.COOKIES.get('session_key')
    if session_key is not None and ValidSession.objects.filter(session_key=session_key).exists():
        usuario_actual = ValidSession.objects.get(session_key=session_key)

    total_mensajes_textuales, total_mensajes_imagenes, total_salas_activas = get_metrics()
    context = {
        'salas': salas_con_mensajes,
        'usuario_actual': usuario_actual,
        'total_mensajes_textuales': total_mensajes_textuales,
        'total_mensajes_imagenes': total_mensajes_imagenes,
        'total_salas_activas': total_salas_activas,
    }

    return render(request, 'home.html', context)


def crear_sala(request):
    if request.method == 'POST':
        session_key = request.COOKIES.get('session_key')
        usuario_actual = ValidSession.objects.get(session_key=session_key).usuario
        nombre = request.POST.get('nombre')

        sala = Sala.objects.filter(nombre=nombre).first()
        if sala:
            sala_url = reverse('sala', args=[sala.nombre])
            return redirect(sala_url)

        sala = Sala.objects.create(nombre=nombre, autor=usuario_actual, session_key=session_key)
        sala_url = reverse('sala', args=[sala.nombre])
        return redirect(sala_url)

    return redirect('home')


@session_required
def help_page(request):
    usuario_actual = None
    session_key = request.COOKIES.get('session_key')
    if session_key is not None and ValidSession.objects.filter(session_key=session_key).exists():
        usuario_actual = ValidSession.objects.get(session_key=session_key)

    total_mensajes_textuales, total_mensajes_imagenes, total_salas_activas = get_metrics()
    context = {
        'usuario_actual': usuario_actual,
        'total_mensajes_textuales': total_mensajes_textuales,
        'total_mensajes_imagenes': total_mensajes_imagenes,
        'total_salas_activas': total_salas_activas,
    }
    return render(request, 'help.html', context)


def handle_image_post_request(request, sala, autor, session_key):
    image_url = request.POST.get('messageText')
    if image_url and validate_url(image_url):
        try:
            response = requests.get(image_url)
            if response.ok:
                mensaje = Mensaje.objects.create(
                    sala=sala,
                    texto=image_url,
                    es_imagen=True,
                    autor=autor,
                    session_key=session_key
                )
                mensaje.save()
            else:
                raise RequestException
        except RequestException:
            mensaje = Mensaje.objects.create(
                sala=sala,
                texto="URL inválida",
                es_imagen=False,
                autor=autor,
                session_key=session_key
            )
            mensaje.save()


def handle_text_post_request(request, sala, autor, session_key):
    message_text = request.POST.get('messageText')
    if message_text:
        mensaje = Mensaje.objects.create(
            sala=sala,
            texto=message_text,
            es_imagen=False,
            autor=autor,
            session_key=session_key
        )
        mensaje.save()


def handle_put_request(request, sala, autor, session_key):
    data = request.body
    try:
        root = ET.fromstring(data)
        for message_elem in root.findall('message'):
            is_image = message_elem.get('isimg')
            text_elem = message_elem.find('text')
            message_text = text_elem.text.strip() if text_elem is not None else None
            if is_image == 'true' and message_text and validate_url(message_text):
                mensaje = Mensaje.objects.create(
                    sala=sala,
                    texto=message_text,
                    es_imagen=True,
                    autor=autor,
                    session_key=session_key
                )
                mensaje.save()
            elif is_image == 'false' and message_text:
                mensaje = Mensaje.objects.create(
                    sala=sala,
                    texto=message_text,
                    es_imagen=False,
                    autor=autor,
                    session_key=session_key
                )
                mensaje.save()
    except ET.ParseError:
        return HttpResponseBadRequest("Error al analizar el documento XML")


def process_request(request, sala, session_key=None):
    mensajes = sala.mensajes.order_by('-fecha')
    valid_password = None
    if session_key is None:
        session_key = request.COOKIES.get('session_key')
    if session_key is not None and ValidSession.objects.filter(session_key=session_key).exists():
        valid_password = ValidSession.objects.get(session_key=session_key)
        autor = valid_password.usuario

        if request.method == 'POST':
            is_image = request.POST.get('isImage')
            if is_image:
                handle_image_post_request(request, sala, autor, session_key)
            else:
                handle_text_post_request(request, sala, autor, session_key)

        if request.method == 'PUT':
            handle_put_request(request, sala, autor, session_key)

    total_mensajes_textuales, total_mensajes_imagenes, total_salas_activas = get_metrics()
    context = {
        'usuario_actual': valid_password,
        'sala': sala,
        'mensajes': mensajes,
        'total_mensajes_textuales': total_mensajes_textuales,
        'total_mensajes_imagenes': total_mensajes_imagenes,
        'total_salas_activas': total_salas_activas,
    }
    return context


@csrf_exempt
@session_required
def room(request, sala_nombre, session_key=None):
    sala = get_object_or_404(Sala, nombre=sala_nombre)
    context = process_request(request, sala, session_key)
    return render(request, 'room.html', context)


@csrf_exempt
@session_required
def dynamic_room(request, sala_nombre):
    sala = get_object_or_404(Sala, nombre=sala_nombre)
    context = process_request(request, sala)
    return render(request, 'dynamic_room.html', context)


def login(request):
    session_key = request.COOKIES.get('session_key')
    if session_key and ValidSession.objects.filter(session_key=session_key).exists():
        return redirect('/')

    if request.method == 'POST':
        form = PasswordForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data.get('password')
            valid_passwords = Passwords.objects.values_list('password', flat=True)
            if password in valid_passwords:
                session_key = secrets.token_hex(16)
                ValidSession.objects.create(usuario='anonimo', session_key=session_key)
                response = redirect('/')
                response.set_cookie('session_key', session_key)
                return response
            else:
                response = render(request, 'login.html', {'form': form})
                response.status_code = 401
                return response
    else:
        form = PasswordForm()

    return render(request, 'login.html', {'form': form})


@session_required
def config(request):
    if request.method == 'POST':
        form = ConfigForm(request.POST)
        if form.is_valid():
            session_key = request.COOKIES.get('session_key')
            usuario_actual = ValidSession.objects.get(session_key=session_key)

            usuario_actual.usuario = form.cleaned_data['charla_name']
            usuario_actual.font_size = form.cleaned_data['font_size']
            usuario_actual.font_type = form.cleaned_data['font_type']
            usuario_actual.save()

            context = {
                'form': form,
                'usuario_actual': usuario_actual,
            }
            return render(request, 'config.html', context)
    else:
        form = ConfigForm()

    session_key = request.COOKIES.get('session_key')
    usuario_actual = None
    if session_key is not None and ValidSession.objects.filter(session_key=session_key).exists():
        usuario_actual = ValidSession.objects.get(session_key=session_key)

    total_mensajes_textuales, total_mensajes_imagenes, total_salas_activas = get_metrics()

    context = {
        'form': form,
        'usuario_actual': usuario_actual,
        'total_mensajes_textuales': total_mensajes_textuales,
        'total_mensajes_imagenes': total_mensajes_imagenes,
        'total_salas_activas': total_salas_activas,
    }
    return render(request, 'config.html', context)


def sala_json(request, sala_nombre):
    sala = get_object_or_404(Sala, nombre=sala_nombre)
    mensajes = sala.mensajes.order_by('-fecha')

    mensajes_json = []
    for mensaje in mensajes:
        mensaje_json = {
            'texto': mensaje.texto,
            'es_imagen': mensaje.es_imagen,
            'autor': mensaje.autor,
            'fecha': mensaje.fecha.strftime("%Y-%m-%d %H:%M:%S"),
        }
        mensajes_json.append(mensaje_json)

    return JsonResponse({'sala': sala.nombre, 'mensajes': mensajes_json})
