# Generated by Django 3.1.7 on 2023-07-03 14:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DeCharla', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='validpassword',
            name='password',
        ),
        migrations.AddField(
            model_name='validpassword',
            name='session_key',
            field=models.CharField(default=1234, max_length=32),
            preserve_default=False,
        ),
    ]
