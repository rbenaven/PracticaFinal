from django import forms
from django.utils.translation import gettext_lazy as _

from DeCharla.models import Sala


class ConfigForm(forms.Form):
    charla_name = forms.CharField(label=_('Nombre de Charlador'), max_length=100)
    font_size = forms.ChoiceField(choices=[('small', _('Pequeña')), ('medium', _('Mediana')), ('large', _('Grande'))])
    font_type = forms.ChoiceField(
        choices=[('arial', _('Arial')), ('times', _('Times New Roman')), ('courier', _('Courier New')),
                 ('fantasy', _('Fantasy'))])


class PasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput)


class CrearSalaForm(forms.ModelForm):
    class Meta:
        model = Sala
        fields = ['nombre']
        labels = {
            'nombre': _('Nombre de la sala')
        }
