from django.contrib import admin
from .models import ValidSession, Sala, Mensaje, Passwords

admin.site.register(ValidSession)
admin.site.register(Sala)
admin.site.register(Mensaje)
admin.site.register(Passwords)