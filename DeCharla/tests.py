from django.test import TestCase, Client
from django.urls import reverse
from .models import ValidSession, Sala, Mensaje, Passwords


class HomeTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_home_page(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='anonimo', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Acceder a la página principal
        response = self.client.get(reverse('home'))

        # Verificar si la página se carga correctamente
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')


class ConfigTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_config_page(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='Mi Charla', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Acceder a la página de configuración
        response = self.client.get(reverse('config'))

        # Verificar si la página se carga correctamente
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'config.html')

        # Realizar una modificación en la configuración
        form_data = {
            'charla_name': 'Mi Nueva Charla',
            'font_size': 'small',
            'font_type': 'fantasy',
        }
        response = self.client.post(reverse('config'), data=form_data)

        # Verificar si los cambios se guardan correctamente
        self.assertEqual(response.status_code, 200)

        # Obtener el objeto ValidSession actualizado
        updated_session = ValidSession.objects.get(session_key=session_key)

        # Verificar si los cambios se reflejan en el objeto ValidSession
        self.assertEqual(updated_session.usuario, 'Mi Nueva Charla')
        self.assertEqual(updated_session.font_size, 'small')
        self.assertEqual(updated_session.font_type, 'fantasy')


class HelpTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_help_page(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='anonimo', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Acceder a la página de ayuda
        response = self.client.get(reverse('help'))

        # Verificar si la página se carga correctamente
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'help.html')


class LoginTest(TestCase):
    def setUp(self):
        self.client = Client()
        password = '12345'
        Passwords.objects.create(password=password)

    def test_login_page(self):
        # Acceder a la página de inicio de sesión
        response = self.client.get(reverse('login'))

        # Verificar si la página se carga correctamente
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_successful_login(self):
        # Realizar una prueba de inicio de sesión exitosa
        response = self.client.post(reverse('login'), {'password': '12345'})

        # Verificar si el inicio de sesión es exitoso
        self.assertRedirects(response, reverse('home'))  # Verificar redirección a la página de inicio
        self.assertIn('session_key', response.client.cookies)  # Verificar si la cookie de sesión se estableció

    def test_failed_login(self):
        # Realizar una prueba de inicio de sesión fallida
        response = self.client.post(reverse('login'), {'password': 'invalid_password'})

        # Verificar si el inicio de sesión falla correctamente
        self.assertEqual(response.status_code, 401)  # Verificar código de estado no autorizado
        self.assertTemplateUsed(response, 'login.html')  # Verificar si se renderiza la plantilla de inicio de sesión
        self.assertNotIn('session_key', response.client.cookies)  # Verificar que no se estableció la cookie de sesión


class CrearSalaTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_crear_sala(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='anonimo', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Crear una sala nueva
        response = self.client.post(reverse('crear_sala'), data={'nombre': 'saladeprueba'})

        # Verificar si la sala se crea correctamente
        self.assertEqual(response.status_code, 302)  # Verificar redirección
        self.assertRedirects(response,
                             reverse('sala', args=['saladeprueba']))  # Verificar redirección a la sala recién creada

        # Verificar si el usuario actual se redirige a la sala recién creada
        response = self.client.get(response.url)
        self.assertEqual(response.status_code, 200)  # Verificar código de estado
        self.assertTemplateUsed(response, 'room.html')  # Verificar si se renderiza la plantilla de la sala
        self.assertIn('saladeprueba',
                      response.content.decode().lower())  # Verificar si el nombre de la sala aparece en la respuesta


class RoomTest(TestCase):
    def setUp(self):
        self.client = Client()
        # Crear una sala antes de las pruebas
        sala_nombre = 'example_sala'
        sala = Sala.objects.create(nombre=sala_nombre)

        # Guardar la sala en una variable de instancia para acceder a ella en las pruebas
        self.sala = sala

    def test_room_page(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='anonimo', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Acceder a una sala específica
        sala_nombre = 'example_sala'
        response = self.client.get(reverse('sala', args=[sala_nombre]))

        # Verificar si la sala se carga correctamente
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'room.html')
        # Verificar otros aspectos de la sala, como los mensajes, etc.

        # Enviar un mensaje en la sala
        response = self.client.post(reverse('sala', args=[sala_nombre]), data={'messageText': 'Hola'})

        # Verificar si el mensaje se envía correctamente
        self.assertEqual(response.status_code, 200)  # Verificar código de estado


class DynamicRoomTest(TestCase):
    def setUp(self):
        self.client = Client()
        # Crear una sala antes de las pruebas
        sala_nombre = 'example_sala'
        sala = Sala.objects.create(nombre=sala_nombre)
        # Puedes agregar más atributos a la sala según sea necesario

        # Guardar la sala en una variable de instancia para acceder a ella en las pruebas
        self.sala = sala

    def test_dynamic_room_page(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='anonimo', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Acceder a una sala dinámica específica
        sala_nombre = 'example_sala'
        response = self.client.get(reverse('dynamic_room', args=[sala_nombre]))

        # Verificar si la sala dinámica se carga correctamente
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dynamic_room.html')
        # Verificar otros aspectos de la sala dinámica, como los mensajes, etc.

        # Enviar un mensaje en la sala dinámica
        response = self.client.post(reverse('dynamic_room', args=[sala_nombre]), data={'messageText': 'Hola'})

        # Verificar si el mensaje se envía correctamente
        self.assertEqual(response.status_code, 200)  # Verificar código de estado


class SalaJsonTest(TestCase):
    def setUp(self):
        self.client = Client()
        # Crear una sala antes de las pruebas
        sala_nombre = 'example_sala'
        sala = Sala.objects.create(nombre=sala_nombre)
        # Puedes agregar más atributos a la sala según sea necesario

        # Crear un mensaje en la sala
        mensaje_texto = 'Hola, esto es un mensaje de prueba'
        mensaje = Mensaje.objects.create(sala=sala, texto=mensaje_texto)
        # Puedes agregar más atributos al mensaje según sea necesario

        # Guardar la sala y el mensaje en variables de instancia para acceder a ellos en las pruebas
        self.sala = sala
        self.mensaje = mensaje

    def test_sala_json_page(self):
        # Iniciar sesión
        session_key = 'example_session_key'
        ValidSession.objects.create(usuario='anonimo', session_key=session_key)
        self.client.cookies['session_key'] = session_key

        # Acceder a la página JSON de una sala específica
        sala_nombre = 'example_sala'
        response = self.client.get(reverse('json_room', args=[sala_nombre]))

        # Verificar si la página se carga correctamente
        self.assertEqual(response.status_code, 200)
        # Obtener los mensajes del JSON de respuesta
        response_data = response.json()
        mensajes_obtenidos = response_data['mensajes']

        # Verificar si el mensaje esperado está en los mensajes obtenidos
        mensaje_esperado = {
            'texto': self.mensaje.texto,
            'autor': self.mensaje.autor,
            'es_imagen': self.mensaje.es_imagen,
            'fecha': self.mensaje.fecha.strftime('%Y-%m-%d %H:%M:%S'),
        }
        self.assertIn(mensaje_esperado, mensajes_obtenidos)
